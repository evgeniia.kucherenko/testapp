import React, {createContext, useContext, useState} from 'react';
import * as authService from '../services/authService';

interface User {
  name: string;
}

interface UserContextType {
  user: User | null;
  login: (
    username: string,
    password: string,
  ) => Promise<{success: boolean; message?: string; name?: string}>;
  logout: () => void;
}

const UserContext = createContext<UserContextType | undefined>(undefined);

export const UserProvider: React.FC<{children: React.ReactNode}> = ({
  children,
}) => {
  const [user, setUser] = useState<User | null>(null);

  const login = async (username: string, password: string) => {
    try {
      const {name} = await authService.login({username, password});
      setUser({name});
      return {success: true, name};
    } catch (error) {
      console.error(error);
      return {success: false, message: 'Login failed'};
    }
  };

  const logout = () => {
    setUser(null);
  };

  return (
    <UserContext.Provider value={{user, login, logout}}>
      {children}
    </UserContext.Provider>
  );
};

export const useUser = (): UserContextType => {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error('useUser must be used within a UserProvider');
  }
  return context;
};
