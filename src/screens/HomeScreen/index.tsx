import React, {useCallback} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

import {NavigationProp, useNavigation} from '@react-navigation/native';
import {AppRouteNames, AppStackParams} from '../../navigators/types';

import {useUser} from '../../context/UserContext';
import {AppLayout} from '../../components/AppLayout';
import {homeScreenStyles} from './styles';

export const HomeScreen = () => {
  const {user, logout} = useUser();
  const navigation = useNavigation<NavigationProp<AppStackParams>>();

  const toLogInScreen = useCallback(() => {
    navigation.navigate(AppRouteNames.LogInScreen);
  }, [navigation]);

  const logOut = () => {
    logout();
    toLogInScreen();
  };

  return (
    <AppLayout withHeader={false}>
      <TouchableOpacity onPress={logOut}>
        <Text style={homeScreenStyles.backBtn}>Log Out</Text>
      </TouchableOpacity>
      <View style={homeScreenStyles.textContainer}>
        <Text style={homeScreenStyles.text}>
          Hi {user?.name}, you're logged in.
        </Text>
      </View>
    </AppLayout>
  );
};
