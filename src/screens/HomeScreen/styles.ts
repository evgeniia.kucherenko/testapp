import {StyleSheet} from 'react-native';
import normalize from 'react-native-normalize';

export const homeScreenStyles = StyleSheet.create({
  backBtn: {
    position: 'absolute',
    fontSize: normalize(16),
    lineHeight: normalize(24),
    color: '#FFC612',
    right: normalize(1),
    top: normalize(-55),
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  text: {
    fontSize: normalize(22),
    fontWeight: '500',
    lineHeight: normalize(27),
    color: '#1F1D1D',
    alignSelf: 'center',
  },
});
