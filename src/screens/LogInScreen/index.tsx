import React, {useCallback} from 'react';
import {
  View,
  TouchableWithoutFeedback,
  Keyboard,
  Text,
  TouchableOpacity,
  Alert,
  ScrollView,
} from 'react-native';
import {useFormik} from 'formik';

import {NavigationProp, useNavigation} from '@react-navigation/native';
import {AppRouteNames, AppStackParams} from '../../navigators/types';

import {useUser} from '../../context/UserContext';
import {TextInput} from '../../components/TextInput';
import {BigBottomButton} from '../../components/BigBottomButton';
import {AppLayout} from '../../components/AppLayout';
import {TextLinkButton} from '../../components/TextLinkButton';
import {ValidationSchema} from './validationSchema';
import {logInScreenStyles} from './styles';

export const LoginScreen = () => {
  const navigation = useNavigation<NavigationProp<AppStackParams>>();

  const goToSignUpScreen = useCallback(() => {
    navigation.navigate(AppRouteNames.RegisterScreen);
  }, [navigation]);
  const goToHomeScreen = useCallback(() => {
    navigation.navigate(AppRouteNames.HomeScreen);
  }, [navigation]);
  const {login} = useUser();

  const {handleChange, handleSubmit, values, errors, touched, resetForm} =
    useFormik({
      validationSchema: ValidationSchema,
      initialValues: {
        username: '',
        password: '',
      },
      onSubmit: async ({username, password}) => {
        try {
          await login(username, password);
          goToHomeScreen();
          resetForm();
        } catch (error) {
          Alert.alert('User does not exist!', 'Please, create an account.');
        }
      },
    });

  return (
    <ScrollView>
      <AppLayout title="Log In">
        <TouchableWithoutFeedback
          onPress={() => Keyboard.dismiss()}
          accessible={false}>
          <View style={logInScreenStyles.inputContainer}>
            <TextInput
              title="Username"
              placeholder="Enter your email"
              editable
              autoCorrect={false}
              spellCheck={false}
              value={values.username}
              onChangeText={handleChange('username')}
              errorText={errors.username}
              hasError={touched.username && !!errors.username}
              autoCapitalize="none"
              keyboardType="email-address"
            />

            <TextInput
              title="Password"
              isPassword
              placeholder="Enter your password"
              editable
              autoCorrect={false}
              spellCheck={false}
              value={values.password}
              onChangeText={handleChange('password')}
              errorText={errors.password}
              hasError={touched.password && !!errors.password}
              autoCapitalize="none"
            />
          </View>
        </TouchableWithoutFeedback>
        <TouchableOpacity
          onPress={() => Alert.alert('Info', 'Please, check your email?!')}>
          <Text style={logInScreenStyles.forgotPasswordBtn}>
            Forgot password?
          </Text>
        </TouchableOpacity>
        <View style={logInScreenStyles.btnContainer}>
          <BigBottomButton onPress={handleSubmit}>Log in</BigBottomButton>
          <TextLinkButton
            leftDescription="New User?"
            onPress={goToSignUpScreen}>
            Create Account
          </TextLinkButton>
        </View>
      </AppLayout>
    </ScrollView>
  );
};
