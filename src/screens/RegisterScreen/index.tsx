import React, {useCallback} from 'react';
import {
  View,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  Alert,
} from 'react-native';
import {useFormik} from 'formik';

import {NavigationProp, useNavigation} from '@react-navigation/native';
import {AppRouteNames, AppStackParams} from '../../navigators/types';

import {register} from '../../services/authService';
import {AppLayout} from '../../components/AppLayout';
import {BigBottomButton} from '../../components/BigBottomButton';
import {TextLinkButton} from '../../components/TextLinkButton';
import {TextInput} from '../../components/TextInput';
import {ValidationSchema} from './validationSchema';
import {signUpScreenStyles} from './styles';

export const RegisterScreen = () => {
  const navigation = useNavigation<NavigationProp<AppStackParams>>();

  const toLogInScreen = useCallback(() => {
    navigation.navigate(AppRouteNames.LogInScreen);
  }, [navigation]);

  const {handleChange, handleSubmit, values, errors, touched} = useFormik({
    validationSchema: ValidationSchema,
    initialValues: {
      name: '',
      username: '',
      password: '',
    },
    validateOnBlur: true,
    validateOnChange: true,
    onSubmit: ({name, username, password}) => {
      const {name: registeredName} = register({
        name,
        username,
        password,
      });
      Alert.alert('Success', `Hi ${registeredName}, you're registered.`);

      toLogInScreen();
    },
  });

  const hasErrorEmail = touched.username && !!errors.username;
  const hasErrorPassword = touched.password && !!errors.password;
  const hasErrorName = touched.name && !!errors.name;

  return (
    <ScrollView>
      <AppLayout title="Register">
        <TouchableWithoutFeedback
          onPress={() => Keyboard.dismiss()}
          accessible={false}>
          <View style={signUpScreenStyles.inputsContainer}>
            <TextInput
              title="Name"
              placeholder="Enter your name"
              editable
              autoCorrect={false}
              spellCheck={false}
              value={values.name}
              onChangeText={handleChange('name')}
              errorText={errors.name}
              hasError={hasErrorName}
              autoCapitalize="none"
            />
            <TextInput
              title="Username"
              placeholder="Enter your email"
              editable
              autoCorrect={false}
              spellCheck={false}
              value={values.username}
              onChangeText={handleChange('username')}
              errorText={errors.username}
              hasError={hasErrorEmail}
              autoCapitalize="none"
              keyboardType="email-address"
            />
            <TextInput
              title="Password"
              isPassword
              placeholder="Enter your password"
              editable
              autoCorrect={false}
              spellCheck={false}
              value={values.password}
              onChangeText={handleChange('password')}
              errorText={errors.password}
              hasError={hasErrorPassword}
              autoCapitalize="none"
            />
          </View>
        </TouchableWithoutFeedback>

        <BigBottomButton onPress={handleSubmit}>Register</BigBottomButton>
        <TextLinkButton leftDescription="Have Account?" onPress={toLogInScreen}>
          Log in
        </TextLinkButton>
      </AppLayout>
    </ScrollView>
  );
};
