import {StyleSheet} from 'react-native';
import normalize from 'react-native-normalize';

export const signUpScreenStyles = StyleSheet.create({
  inputsContainer: {
    marginTop: normalize(30),
  },
});
