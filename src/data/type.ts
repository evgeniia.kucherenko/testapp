export interface Login {
  username: string;
  password: string;
}

export interface Register extends Login {
  name: string;
}

export interface User extends Register {
  id: string;
}
