import {User} from './type';

export const mockData: User[] = [
  {
    id: '1',
    name: 'Kate',
    username: 'test@gmail.com',
    password: '123456',
  },
  {
    id: '1',
    name: 'Alex',
    username: 'alex@gmail.com',
    password: 'Password',
  },
  {
    id: '1',
    name: 'Ira',
    username: 'ira@gmail.com',
    password: 'qwerty',
  },
];
