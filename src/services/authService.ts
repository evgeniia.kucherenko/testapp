import {mockData} from '../data/mockData';
import {Login, Register} from '../data/type';

const mockJWT = 'fake-jwt-token';

export const login = ({username, password}: Login) => {
  const user = mockData.find(
    user => user.username === username && user.password === password,
  );
  if (user) {
    return {token: mockJWT, name: user.name};
  } else {
    throw new Error('Authentication failed');
  }
};

export const register = ({name, username, password}: Register) => {
  const existingUser = mockData.find(user => user.username === username);
  if (existingUser) {
    throw new Error('User already exists');
  } else {
    const newUser = {id: Date.now().toString(), name, username, password};
    mockData.push(newUser);
    return {token: mockJWT, name: name};
  }
};
