export enum AppRouteNames {
  LogInScreen = 'LogInScreen',
  HomeScreen = 'HomeScreen',
  RegisterScreen = 'RegisterScreen',
}

export type AppStackParams = {
  navigate(LogInScreen: AppRouteNames): void;
  [AppRouteNames.LogInScreen]: undefined;
  [AppRouteNames.HomeScreen]: undefined;
  [AppRouteNames.RegisterScreen]: undefined;
};
