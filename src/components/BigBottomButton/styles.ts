import {StyleSheet} from 'react-native';
import normalize from 'react-native-normalize';

export const bigBottomButtonStyles = StyleSheet.create({
  containerBtn: {
    backgroundColor: '#FFC612',
    width: '100%',
    paddingVertical: normalize(17),
    borderRadius: normalize(20),
    marginTop: normalize(50),
  },
  titleBtn: {
    fontSize: normalize(18),
    lineHeight: normalize(27),
    alignSelf: 'center',
    color: '#1F1D1D',
  },
});
