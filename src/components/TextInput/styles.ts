import {StyleSheet} from 'react-native';
import normalize from 'react-native-normalize';

export const textInputStyles = StyleSheet.create({
  container: {
    marginVertical: normalize(20),
    alignSelf: 'stretch',
    width: '100%',
  },
  title: {
    fontSize: normalize(14),
    lineHeight: normalize(21),
    color: '#1F1D1D',
    marginBottom: normalize(3),
  },
  textInput: {
    color: '#1F1D1D',
    paddingVertical: normalize(12),
    borderColor: '#D7D7D7',
    borderBottomWidth: normalize(1),
    fontSize: normalize(16),
    lineHeight: normalize(24),
  },
  disabled: {
    backgroundColor: '#D7D7D7',
  },
  error: {
    borderColor: 'red',
  },
  errorText: {
    color: 'red',
  },
  iconBtn: {
    position: 'absolute',
    right: normalize(2),
    bottom: normalize(12),
  },
});
