import React from 'react';
import {PropsWithChildren} from 'react';
import {Text} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {appLayoutStyles} from './styles';

interface IProps {
  title?: string;
  withHeader?: boolean;
}

export const AppLayout = ({
  title,
  children,
  withHeader = true,
}: PropsWithChildren<IProps>) => {
  return (
    <SafeAreaView style={appLayoutStyles.container}>
      {withHeader && <Text style={appLayoutStyles.title}>{title}</Text>}
      {children}
    </SafeAreaView>
  );
};
