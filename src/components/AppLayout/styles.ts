import {StyleSheet} from 'react-native';
import normalize from 'react-native-normalize';

export const appLayoutStyles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: normalize(100),
    marginHorizontal: normalize(32),
  },
  title: {
    fontSize: normalize(24),
    fontWeight: '600',
    lineHeight: normalize(36),
    alignSelf: 'center',
    color: '#1F1D1D',
  },
});
