import {Platform, StyleSheet} from 'react-native';
import normalize from 'react-native-normalize';

export const textLinkButtonStyles = StyleSheet.create({
  containerBtn: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: normalize(35),
    marginBottom: Platform.OS === 'android' ? 30 : 0,
  },
  description: {
    fontSize: normalize(14),
    lineHeight: normalize(21),
    color: '#9795A4',
    paddingRight: normalize(5),
  },
  titleBtn: {
    fontSize: normalize(14),
    lineHeight: normalize(21),
    color: '#FFC612',
  },
});
